export class ImageLink {
  constructor(title, image, link) {
    this.title = title;
    this.link = link;
    this.image = image;
  }
}
