import firebase from 'firebase';


export const firebaseConfig = {
    apiKey: "AIzaSyDThGSMdqXGtzZJFcFLPYJp65Oe5P9XZ00",
    authDomain: "computingninformation.firebaseapp.com",
    databaseURL: "https://computingninformation.firebaseio.com",
    projectId: "computingninformation",
    storageBucket: "computingninformation.appspot.com",
    messagingSenderId: "191366947595",
    appId: "1:191366947595:web:4b4e3feddf8ec1c564780e"
  };

export const initializeFirebase = () => {
  firebase.initializeApp(firebaseConfig);
}

export const signOut = () => {
  return firebase.auth().signOut();
}

export const signInWithLogin= (email, password) => {
  return firebase
   .auth()
   .signInWithEmailAndPassword(email, password)
   .then((response) => response);
}

export const signInWithGoogle = () => {
  let provider = 
    new firebase
    .auth
    .GoogleAuthProvider()
    .addScope('https://www.googleapis.com/auth/contacts.readonly');
  return firebase.auth().signInWithPopup(provider);
}
